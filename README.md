## Сборщик фронтенда

Gulp, Twig, SCSS, Babel

### Команды
`yarn install` — установить зависимости

`gulp` — запустить девсервер

`gulp build` — собрать билд без подключения девсервера

`gulp createBlock --name 'header, footer, etc'` — создать типовые блоки

—

Игорь Петров, 2018 — …
